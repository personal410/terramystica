//
//  Player.swift
//  TerraMystica
//
//  Created by victor salazar on 4/2/18.
//  Copyright © 2018 victor salazar. All rights reserved.
//
import UIKit
class Player{
    var name:String
    var color:UIColor
    init(_ name:String, _ color:UIColor){
        self.name = name
        self.color = color
    }
}

class FactionPlayer:Player {
    var faction:String
    init(_ name:String, _ color:UIColor, _ faction:String) {
        self.faction = faction
        super.init(name, color)
    }
}
