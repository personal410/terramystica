//
//  PlayerTableViewCell.swift
//  TerraMystica
//
//  Created by victor salazar on 4/2/18.
//  Copyright © 2018 victor salazar. All rights reserved.
//
import UIKit
class PlayerTableViewCell:UITableViewCell {
    //MARK: - IBOutlet
    @IBOutlet weak var lblPlayerName:UILabel!
    @IBOutlet weak var lblColorFaction:UILabel!
    //MARK: - Auxiliar
    func configure(_ player:Player){
        lblPlayerName.text = player.name
        lblColorFaction.backgroundColor = player.color
        if let factionPlayer = player as? FactionPlayer {
            lblColorFaction.text = factionPlayer.faction
        }
    }
}
