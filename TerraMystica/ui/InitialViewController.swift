//
//  ViewController.swift
//  TerraMystica
//
//  Created by victor salazar on 4/2/18.
//  Copyright © 2018 victor salazar. All rights reserved.
//
import UIKit
class InitialViewController:UIViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewCont = segue.destination as! NewGameViewController
        viewCont.type = sender as! NewGameType
    }
    //MARK: - IBAction
    @IBAction func newGame(){
        let alertCont = UIAlertController(title: "Nuevo Juego", message: "Escoge el modo de juego", preferredStyle: .actionSheet)
        alertCont.addAction(UIAlertAction(title: "Color", style: .default, handler: { (action) in
            self.performSegue(withIdentifier: "showNewGame", sender: NewGameType.Color)
        }))
        alertCont.addAction(UIAlertAction(title: "Facción", style: .default, handler: {(action) in
            self.performSegue(withIdentifier: "showNewGame", sender: NewGameType.Faction)
        }))
        alertCont.addAction(UIAlertAction(title: "Cancelar", style: .destructive, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
}
