//
//  NewGameViewController.swift
//  TerraMystica
//
//  Created by victor salazar on 4/2/18.
//  Copyright © 2018 victor salazar. All rights reserved.
//
import UIKit

enum NewGameType:Int{
    case Color, Faction
}

class NewGameViewController:UIViewController, UITableViewDataSource {
    //MARK: - Constants
    let arrColors = [UIColor(hex: "CECECE"), UIColor(hex: "EE1B23"), UIColor(hex: "FFF200"), UIColor(hex: "B97A57"), UIColor(hex: "5B5B5B"), UIColor(hex: "00A3E9"), UIColor(hex: "23B14C")]
    let arrFactions = [["Ingenieros", "Enanos"], ["Magos del Caos", "Gigantes"], ["Faquires", "Nómadas"], ["Medianos", "Cultistas"], ["Alquimistas", "Moradores de la Oscuridad"], ["Sirenas", "Moradores de las Aguas"], ["Auren", "Brujas"]]
    //MARK: - Properties
    var type:NewGameType!
    var arrPlayers:[Player] = []
    var arrIndexColors:[Int] = []
    //MARK: - IBOutlet
    @IBOutlet weak var playersTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //MARK: - IBAction
    @IBAction func addPlayer(){
        if arrPlayers.count < 5 {
            let alertCont = UIAlertController(title: "Nuevo Jugador", message: nil, preferredStyle: .alert)
            alertCont.addTextField {(txtFld) in
                txtFld.placeholder = "Jugador \(self.arrPlayers.count + 1)"
                txtFld.autocapitalizationType = .sentences
            }
            alertCont.addAction(UIAlertAction(title: "Agregar", style: .default, handler: { (action) in
                let txtFld = alertCont.textFields!.first!
                let playerName = txtFld.text!.isEmpty ? txtFld.placeholder! : txtFld.text!
                let player:Player
                if self.type == .Color {
                    player = Player(playerName, self.arrColors[self.generateRandomNumber()])
                }else{
                    let index = self.generateRandomNumber()
                    let factions = self.arrFactions[index]
                    let indexFaction = Int(arc4random() % 2)
                    player = FactionPlayer(playerName, self.arrColors[index], " \(factions[indexFaction]) ")
                }
                self.arrPlayers.append(player)
                self.playersTableView.reloadData()
            }))
            alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }else{
            let alertCont = UIAlertController(title: "Alerta", message: "No puede haber más de 5 jugadores", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }
    }
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlayers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as! PlayerTableViewCell
        cell.configure(arrPlayers[indexPath.row])
        return cell
    }
    //MARK: - Auxiliar
    func generateRandomNumber() -> Int {
        var number = -1
        while number == -1 {
            let randomNumber:Int = Int(arc4random() % 7)
            if arrIndexColors.index(of: randomNumber) == nil {
                number = randomNumber
                arrIndexColors.append(number)
            }
        }
        return number
    }
}
